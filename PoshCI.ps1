﻿#Import-Module C:\Users\Build\Documents\WindowsPowerShell\posh-git\src\posh-git.psd1
$env:GIT_REDIRECT_STDERR = '2>&1'

$WorkDir = "C:\Users\Build\Desktop\BuildMyRep"
# Парамсы передаваемые при сборке приложения
$BuildBin = "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\MSBuild.exe"
$VersionApp = "1.0.0.0"
$AssemblyName ="SemDrCSharpProject"
$BuildParams ="-p:AssemblyName=$AssemblyName" 

# Парамсы передаваемые при создании Nuget пакета приложения
$NugetPacker = $WorkDir + "\nuget.exe"
$NugetParams = "pack"
$NuspecFile = $WorkDir + "\my.nuspec"
$nugetServer = "https://api.nuget.org/v3/index.json"
$ApiKey = "oy2gangrzgqwtxlpvw7ahehlj5h5lrheze5a4btipop25e"

# Git
$GitRepository = "git@gitlab.com:DrSema/kontur-exploitation.git" # URL репозитория 
$CheckTime = 120 # Интервал проверки обновлений git в секундах, 
#после публикации какое то время опубликованный пакет не видно и используется уже используемая версия
# что приврдит к ошибке при пуше на Nuget.org

$OutputLogComleteApk = "bin\Debug\Output.txt" # Лог собранного приложения


# проверка обновления и запуск билда
function CheckGitUpdate
{
cd $WorkDir
#Remove-Item ".\kontur-exploitation\" -Recurse -Force -ErrorAction SilentlyContinue

# Получаем имя каталога куда должен скопироваться реп, в промежутке от "/" до ".git"
$Repdir = [Regex]::Matches($GitRepository,'(?<=\/).*?(?=\.git)')-join';'
# Существует ли каталог
if ((Test-Path $repdir)) 
    {
    #Да git clone не требуется, едем дальше
    Write-Host "Каталог найден"
    }
Else
    {
    # Нет такого каталога, git clone требуется
    git clone $GitRepository
    }
# Переходим в каталог с репом
cd $Repdir
#Получаем имена всех веток репозитория
$branch = git branch -a 
#Определякм имена веток

for ($i=0; $i -lt $branch.Count; $i++)
    {
        $branch_name = $branch.Item($i) # общее кол-во показанного git branch
    if ($branch_name.Contains("*") ) { Continue } # Игнорируем строки содержащие * HEAD
    if ($branch_name.Contains("HEAD") ) { Continue }
    if (!$branch_name.Contains("remotes")) { Continue } # Используем ветки только серверного репозитория
    #Удаляем лишние символы 
      $branch_name = $branch.Item($i).Replace("remotes/origin/","").Replace("  ","") | %{ $_.Split(' ')[0];}
      #Выбираем ветку
      Write-Host "Выбираем $branch_name"
      git checkout $branch_name
      #Стягиваем изменения и если они есть запускаем сборку
      $git_pull_out = git pull
      if ( $git_pull_out.Contains("Already up to date.") )
        {
        Write-Host "Ветка $branch_name без изменений"
        }
      #Найдены изменения в ветке
      Else 
        {
        Write-Host $branch_name "Есть изменения, запускаем сборку ветки"
        RunBuildApp ($branch_name, $Repdir)
        }

    }

#RunBuildApp ("test", $Repdir) #для отладки сбоки чтоб не комиттить каждый раз
}

# билд и запуск сборки
function RunBuildApp ($build_branch_name)
{  
    Write-Host "Сборка" $branch_name
    # Выбираем ветку
    git checkout $build_branch_name
    # проверка актуальности версии
    $VersionApp = (CheckVer $packageName $VersionApp)
    # Задаем версию сборки
    ConfigEdit ".\Properties\AssemblyInfo.cs" "Version" $VersionApp
    #ConfigEdit "..\my.nuspec" "Version" $VersionApp
       
    $OutDir="bin\Debug"
    $BinAppp=$OutDir + "\" +$AssemblyName + ".exe"
    # Запуск сборки
    & $BuildBin $BuildParams
    # Запускаем собранное приложение с перенаправлением вывода в файл $OutputLogComleteApk
    & $BinAppp | out-file $OutputLogComleteApk

    # Собираем Nuget пакет
    PackNuget $VersionApp
}

# упаковка в пакет 
function PackNuget($VersionApp)
{  
   # Хэш последнего коммита
   $hash_commit = git log -1 --pretty="format:%H"
   #Автор послед коммита Имя %an Почта %ae
   $Authors = git log -1 $hash_commit --pretty="format:%an %ae"
   #консольный вывод приложения и хеш последнего коммита
   $Description = (Get-Content $OutputLogComleteApk) + "`nHash last commit:" + $hash_commit
   
   # проверка актуальности версии, перенес проверку в билд приложения
   #$VersionApp = (CheckVer $packageName $VersionApp)
   
   # Создадим xml c параметрами сборки пакета
   $nuspecFile = "nuspecFile.nuspec"
   GenerateXML $AssemblyName $VersionApp $Description $Authors $nuspecFile | out-file $nuspecFile
   
   # удалить все существующие файлы *.nupkg из каталога
   get-childitem | where {$_.extension -eq ".nupkg"} | foreach ($_) {remove-item $_.fullname}


   # Запускаем сборку
   & $NugetPacker $NugetParams $nuspecFile

   # Пушим образ в репозеторий
   PushNuget $AssemblyName $VersionApp
}

# отправка свежеиспеченного пакета
function PushNuget($packageName)
{
    # Ищкм пакет в текущем каталоге
    $package = get-childitem | where {$_.extension -eq ".nupkg"}
    
    #& $NugetPacker "setApiKey" $ApiKey  # разовая операция добавления Api токена
    # Отправка пакета
    & $NugetPacker "push" $package $ApiKey "-Source" $nugetServer
}  

# Проверялка корректности версии
function CheckVer($packageName, $VersionApp)
{
$vers = $VersionApp
# Ищем опубликованные пакеты если они есть последнюю увеличиваем на 1
$latestRelease = (& $NugetPacker "list" $packageName)
# Проверяем найдны ли пакеты с таким именем
if ($latestRelease -match "не найдены")
    {
    Write-host "Не найдено"
    # Дефолтная версия норм ее и возвращаем без изменений
    }

else 
    {
    # увеличиваем версию
    $version = $latestRelease.split(" ")[1];
    $versionTokens = $version.split(".")
    $buildNumber = [System.Double]::Parse($versionTokens[$versionTokens.Count -1]) 
    $versionTokens[$versionTokens.Count -1] = $buildNumber +1
    $newVersion = [string]::join('.', $versionTokens)
    # Возвращаем новую версию
    $vers=$newVersion
    Write-host "найдено"
    }
 
return $vers
}
 
# редактирование AssemblyInfo.cs проекта 
function ConfigEdit($ConfigPath, $Param, $ValueParam)
{
    $new_val = $Param+'("{0}")' -f "$ValueParam"
    $content = Get-Content $ConfigPath
    if ($ConfigPath -match "[.cs]$") {$replace_val=$Param+'\(".*"\)'}
    #if ($ConfigPath -match "[.nuspec]$") {$replace_val='\'+$Param+'>'+'.*\'+'<'+$Param}
        #PS не умеет регекспы с символами "> <" затея с регекспами на xml провалилась
        # а для методов [xml] нужно указывать только полные пути к файлу, с относительными не работает, жесские ссылки не наш путь
    Write-Host $replace_val
    $content -replace $replace_val,$new_val > $ConfigPath

}

# xml заготовка для nuget пакета
function GenerateXML($AssemblyName, $VersionApp, $Description, $Authors)
{
$data = @"
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
    <metadata>
        <id>$AssemblyName</id>
        <version>$VersionApp</version>
        <license type="expression">MIT</license>
        <licenseUrl>https://licenses.nuget.org/MIT</licenseUrl>
        <description>$Description</description>
        <authors>$Authors</authors>
    </metadata>
    <files>
        <file src="bin\Debug\**" target="bin\" />
    </files>
</package>
"@

return $data
}


# Проверка обновлений в безконечном цикле
While(1)
{
    CheckGitUpdate
    Start-Sleep -Seconds $CheckTime
}