# PowerShell CI for .Net

### Тестовое задание на PowerShell / C#  
git@github.com:kontur-exploitation/testcase-posharp.git  

**Рабочее окружение:**  
Win 10 1809  
Visual Studio buildtools  
NDP472-DevPack-ENU (Net Framework 4.7.2 Developer Pack)  
Git-2.22.0-64-bit  
NuGet Command Line  

В рабочем каталоге скрипта $WorkDir должна ноходится утилита nuget.exe  
**Основные настройки описываются в начале скрипта **  
$WorkDir Рабочий каталог скрипта, сюда будет копироваться репозиторий 
и в каталоге репозитория будут происходить все сборки   
# Парамсы передаваемые при сборке приложения  
$BuildBin путь до MSBuild.exe  
$VersionApp минимально возможная версия приложения  
$AssemblyName Имя собираемого приложения  
$BuildParams параметры запуска MSBuild.exe  

# Парамсы передаваемые при создании Nuget пакета приложения  
$NugetPacker путь до nuget.exe  
$NugetParams параметры запуска nuget.exe  
$NuspecFile файл для создания nuget пакета  
$nugetServer = "https://api.nuget.org/v3/index.json"  
$ApiKey = "oy2gangrzgqwtxlpvw7ahehlj5h5lrheze5a4btipop25e"  
*Ключик удалю после того как задание будет проверенно))*  

# Git  
$GitRepository = "git@gitlab.com:DrSema/kontur-exploitation.git" # URL репозитория  
$CheckTime = 120 # Интервал проверки обновлений git в секундах,   
#после публикации какое то время опубликованный пакет не видно в "nuget list".  
#При сборке приложения начинает используется уже используемая версия  
#что приврдит к ошибке при пуше на Nuget.org  

$OutputLogComleteApk = "bin\Debug\Output.txt" # Лог собранного приложения по т.з.  

В целом код довольно подробно откоментирован  
Скрипт может работать из любого места в фс  
сторонние модули не используюся  